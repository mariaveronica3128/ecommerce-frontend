import React from 'react';
import {Row, Carousel,} from 'react-bootstrap';


export default function Banner () {

    return (
        <Row>
				<Carousel>
  <Carousel.Item className= "carousel" interval={3000}>
    <img
     className="d-block w-100" 
     src={require("../images/T61.1.jpg")} 
     alt="First slide" 
     width="100%" 
     height="600vh"
    />
  </Carousel.Item>

  <Carousel.Item className= "carousel" interval={2000}>
    <img
     className="d-block w-100" 
     src={require("../images/T63.1.jpg")} 
     alt="Second slide" 
     width="100%" 
     height="600vh"
    />
  </Carousel.Item>

  <Carousel.Item  interval={2000}>
    <img
     className="d-block w-100" 
     src={require("../images/T9.1.jpg")} 
     alt="Third slide" 
     width="100%" 
     height="600vh"
    />
  </Carousel.Item>
</Carousel>
			</Row>
		

    )
}