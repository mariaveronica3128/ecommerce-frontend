import React from 'react';
import {Row, Col} from 'react-bootstrap';

export default function HomeBanner({bannerData}) {
    return(
        <Row className='p-5'>
        <Col className='text-center'>
        <h1> {bannerData.title}</h1>
        <h5 className='my-3'>{bannerData.content}</h5>
        <a className='button' href='/Products'>SHOP NOW</a>  
        </Col>
        </Row>
        );
    };
    
