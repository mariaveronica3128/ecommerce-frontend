import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddCProducts({fetchData}) {

	//Add state for the forms of product
	const [ProductName, setProductName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [color, setColor] = useState('')

	//States for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	//Functions to handle opening and closing of our Addproduct Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false)



	//Function for fetching the create product in the backend
	const AddCProducts = (e) => {
		e.preventDefault();

		fetch('https://warm-shelf-88809.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				ProductName: ProductName,
				description: description,
				color: color,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				//Show a success message
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product successfully added.'
				})

				//Close our modal
				closeAdd();
				//Render the updated data using the fetchData prop
				fetchData();
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: 'Something went wrong. Please Try again'
				})
				closeAdd();
				fetchData();
			}

			//Clear out the input fields
			setProductName("")
			setDescription("")
			setColor("")
			setPrice("")


		})
	}


	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New product</Button>

		{/*Add Modal Forms*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => AddCProducts(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={ProductName} onChange={e => setProductName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Color</Form.Label>
							<Form.Control type="text" value={color} onChange={e => setColor(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>

		)
}



