import styled from 'styled-components';

export const Box = styled.div`
padding: 0;
background: black;
position: bottom;
bottom: 0;
width: 100%;


@media (max-width: 1000px) {
	padding: 70px 30px;
}
`;

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: end;
	max-width: 1000px;
	margin: 0 auto;
	
`

export const Column = styled.div`
display: flex;
flex-direction: row;
text-align: left;
margin-left: 60px;
`;

export const Row = styled.div`
display: grid;
grid-template-columns: repeat(auto-fill,
						minmax(185px, 1fr));
grid-gap: 10px;

@media (max-width: 1000px) {
	grid-template-columns: repeat(auto-fill,
						minmax(200px, 1fr));
}
`;

export const FooterLink = styled.a`
color: #fff;
margin-bottom: 0px;
font-size: 18px;
text-decoration: none;

&:hover {
	color: green;
	transition: 200ms ease-in;
}
`;

export const Heading = styled.p`
font-size: 25px;
color: #fff;
margin-bottom: 0px;
font-weight: bold;
`;

export const Footertext = styled.p`
color:#fff;
text-align: center;
`
