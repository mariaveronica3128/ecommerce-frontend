import React from 'react';
import { Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';



export default function ProductCard ({productProp}) {
   
    const { _id, ProductName, description, price} = productProp
    console.log(productProp)
    // const [currentValue(getter), updateValue(setter)] = useState(InitialGetterValue)

    // const [isOpen, setIsOpen] = useState(true)

    //     const [count, setCount] = useState(0)
    //     const [seat, setSeat] = useState(30)
    //     // console.log(count)
    //     // console.log(seat)

    //     const enroll = () => {
           
    //             setCount(count + 1);
    //             console.log("Enrollees: " + count);
    //             setSeat(seat -1)
    //             console.log("Seat: " + seat);     
    //     }

    //     useEffect(() => {
    //         if (seat === 0) {
    //             setIsOpen(false)
    //         }
    //     }, [seat]) 

    return (
               
        <Card className="mt-5 mx-3 d-md-inline-flex d-sm-inline-flex cardForm text-center w-60" border = "warning">
        <Card.Body className='overflow-hidden' > 
        <Card.Title className='text-center'>
        {ProductName}
        </Card.Title>
        <Card.Text class >{description}</Card.Text>
        </Card.Body>
        <Card.Text className='pl-2 text-center'>  {`Price:\u20B1${price}`} </Card.Text>

            <Link className='btn btn-dark' to={`/products/${_id}`}>View products</Link>
       
    </Card>
         
    )
}

ProductCard.propTypes = {
    productProp: PropTypes.shape ({
        ProductName: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
