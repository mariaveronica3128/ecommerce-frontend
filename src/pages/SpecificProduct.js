import React, { useState, useContext, useEffect } from 'react';
import {Container,Card,Button} from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {useParams, useNavigate, Link} from 'react-router-dom';

export default function SpecificProduct(){
    const { productId } = useParams();
	const navigate = useNavigate();
    
    const {user} = useContext(UserContext);
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
    

    useEffect(() => {
		fetch(`https://warm-shelf-88809.herokuapp.com/products/${productId}/`, {
			method:'GET',
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setProductName(data.productName)
			setDescription(data.description)
			setPrice(data.price)
            
		})
	}, [])
    
    return(
        <Container>
			<Card className="mt-4">
				<Card.Header>
					<h4>{productName}</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>
				<Card.Footer>
				{user.accessToken !== null ?
					<div className="d-grip gap-2">
						<Button variant="primary">Add to Cart</Button>
					</div>		
					:
					<Link className="btn btn-warning d-grip gap-2" to="/login">Login to Purchase</Link>
				}
				</Card.Footer>
			</Card>
		</Container>
    )
}
