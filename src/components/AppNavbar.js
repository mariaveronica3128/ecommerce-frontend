import React from 'react';
import { Navbar, Nav, Container, NavDropdown} from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import {  faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import img1 from '../images/brand.png'

function AppNavBar (){

    const  { user } =  useContext(UserContext)  ;
   
    return(
    <Navbar bg="dark" expand="lg">
     <Container>
    
        <Navbar.Brand className='text-white'>
        <img src={img1} className='img-fluid LOGO m-2 ' width={50} alt="shoes"></img>
            Graphix Tee</Navbar.Brand>
        <Navbar.Toggle aria-label="basic-navbar-nav"/>
        
        {(user.isAdmin === true ?
         <>
         <NavDropdown  title={
             <FontAwesomeIcon icon="fa-sign-out-alt" />   
             } id="nav-dropdown">
             <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
             <NavDropdown.Divider />
          
         </NavDropdown>  
        
         
         </>

        :
         
     <Navbar.Collapse>

            <Nav className='ms-auto'>
               <Link to="/" className=' nav nav-link text-white'>Home</Link>
               <Link to="/products" className='nav-link text-white'>Products</Link>
             

               {user.accessToken  !== null ? 
               <>
                <NavDropdown  title={
                    <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon>      
                    } id="nav-dropdown">
                    {/* <NavDropdown.Item href="/cart">My Cart</NavDropdown.Item> */}
                    <NavDropdown.Item href="/order">My Orders</NavDropdown.Item>
                    <NavDropdown.Item href="/cart">My Cart</NavDropdown.Item>
                    <NavDropdown.Divider />                   
                </NavDropdown>  
                <Link to="/logout" className='nav-link text-white mx-3'>Logout</Link>
                </>
               :
               <>
               <Link to="/login" className='nav-link text-white'>Login</Link>
               <Link to="/register" className='nav-link text-white'>Register</Link>
               </>
               }
            </Nav>   
        </Navbar.Collapse>
        )}

     </Container>
   
    </Navbar>
        );
    };
    export default AppNavBar;
    
