import React from "react";

import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Footer from "../components/Footer";
// import UserContext from '../UserContext';
// import { useContext } from 'react';
import HomeBanner from "../components/HomeBanner";


const data = {
   

    title: 'MORE UNIQUE DESIGN COMING!',
    content: 'Grab yours at 10% Off upon purchasing your first item!'
}

export default  function Home(){
   

    return (
        
    
        <>
        <HomeBanner bannerData={data}/>
        <Banner/>
        <Highlights/>
        <Footer/>
        </>
       
       
        
    );
};
