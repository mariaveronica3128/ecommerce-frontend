import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteProduct({product, fetchData}) {

	const deleteProduct = (productId) => {
			
			fetch(`https://warm-shelf-88809.herokuapp.com/products/${productId}`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'product successfully deleted'
					})
					fetchData()
			})

	}

	return(

		<>
			<Button className= "btnDanger" variant="danger" size="sm" onClick={() => deleteProduct(product)}>Delete</Button>

				
		</>

		)
}
