import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchivedProducts({product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
		fetch(`https://warm-shelf-88809.herokuapp.com/products/archive/${productId}/`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'product successfully disabled'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}

	const unarchiveToggle = (productId) => {
		fetch(`https://warm-shelf-88809.herokuapp.com/products/unarchive/${productId}/`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "success!!",
					icon: "success",
					text: "Successfully Enabled"
				})

				fetchData();

			} else{
				Swal.fire({
					title: "Oooops!!",
					icon: "error",
					text: "Something's Wrong. Try Again"
				})

				fetchData();
				
			}
		})
	}

 

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => unarchiveToggle(product)}>Unarchive</Button>

			}
		</>

		)
}