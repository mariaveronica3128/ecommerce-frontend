import React, { useState, useEffect, useContext} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import { Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register () {

    const { user } = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
  

    const [isActive, setIsActive] = useState (true)
    const history = useNavigate()

    useEffect(() => {
        if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2 
            )) {
            setIsActive(true)
        }else {
            setIsActive(false)
        }
    }, [email, password1, password2])

    function registerUser (e) {
        e.preventDefault();

			fetch('https://warm-shelf-88809.herokuapp.com/users/register', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
                   
					email: email,
                  
					password: password1

				})
			})
			.then(res => res.json())
			.then(data => {

			if (data === true) {

				setEmail('');
				setPassword1('');
              

				Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "You can now login",
					})
				history("/login")
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
						confirmButtonColor: 'red'
					})
				}
			})
		
	}

    return (
        // conditional rendering
        (user.accessToken !== null) ?

        <Navigate to="/" />
        :
        <Container className='mt-5'>
        <Form onSubmit = {(e) => registerUser(e)} id='registerForm' className='border p-5 registerForm mt-2'>
            <h1 className='text-center'>Register</h1>
            <Form.Group>
                <Form.Label>Email Address</Form.Label>
                <Form.Control  className='formRegister shadow-lg'
                type= "email"
                placeholder="Enter email"
                required
                value= {email}
                onChange= {e => setEmail(e.target.value)}
                />
                <Form.Text className= "text-muted">
                    We'll never share your email with anyone
                </Form.Text>   
            </Form.Group>

            <Form.Group className='mt-3'>
                <Form.Label >Password</Form.Label>
                <Form.Control
                type= "password"
                placeholder="Enter password"
                required
                value={password1}
                onChange= {e => setPassword1(e.target.value)}
                /> 
            </Form.Group>

            <Form.Group>
                <Form.Label className='mt-3'>Verify Password</Form.Label>
                <Form.Control
                className='formRegister shadow-lg'
                type= "password"
                placeholder="Verify Password"
                required
                value={password2}
                onChange= {e => setPassword2(e.target.value)}
                />  
            </Form.Group>

           
            {isActive ?
            <Button variant="danger" type="submit" className="mt-3">Submit</Button>
            :
            <Button variant="danger" type="submit" className="mt-3" disabled>Submit</Button>
            }
            <Form.Group className='text-center'>
                <Form.Text>Are you already register ? </Form.Text> <br/>
			<Form.Text as={Link} to="/login"><Button>SignUp Here</Button></Form.Text>
			</Form.Group>

            
        </Form>
        </Container>
    )
}


