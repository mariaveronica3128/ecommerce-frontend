import React from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function OrderCard({orderProp}) {
	const {productName,description, price} = orderProp;
    console.log(orderProp)

	return(
		<Card>
			<Card.Body>
				<Card.Title>{productName}</Card.Title>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Card.Subtitle>Quantity:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Button variant="primary">Checkout</Button>
			</Card.Body>
		</Card>
		);
}

OrderCard.propTypes = {
	orderProps: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
