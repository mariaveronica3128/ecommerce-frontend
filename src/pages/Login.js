import React, { useState, useEffect, useContext } from 'react';
//useContext is used to unpack or deconstruct the value of the UserContext
import { Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
// import App from './App.css'


import { Navigate, useNavigate, Link } from 'react-router-dom';

export default function Login() {

	const navigate = useNavigate();


	const { user, setUser } = useContext(UserContext);


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button 
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])


	function authenticate(e) {
		e.preventDefault();

		fetch('https://warm-shelf-88809.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					title:'Yay!',
					icon: 'success',
					text: 'Successfully Logged in'
				})

				//Getting the user's credentials
				fetch('https://warm-shelf-88809.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						//redirect the admin to /products
						navigate('/Products')

					}else {
						//if not an admin, redirect to homepage
						navigate('/')
					}

				})

			}else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}

			setEmail('');
			setPassword('');

		})
	}
	

	return (
		// Create a conditional rendering statement that will redirect the user to the products page when a user is logged in.

		(user.accessToken !== null) ?

		<Navigate to="/Products" />

		:
		<div>
		 <Container className='mt-5  ' >
		<Form onSubmit={(e) => authenticate(e)}>
		<h1 className='text-center'>Login</h1>
		<div className='border container mt-2' id='formBox'>
			<Form.Group className='mt-5'>
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={(e) => setEmail(e.target.value)}

				/>
			</Form.Group>

			<Form.Group className='mt-2'>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your password"
					required
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					
				/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3 mb-3" > Login </Button>

				:

				<Button variant="danger" type="submit" disabled className="mt-3 mb-3"> Login </Button>
			}
     </div>
			
		</Form>
		<p className='mt-3' class='text-center'>Not registered yet?<Link to="/register"> 
          Click to Register
         </Link></p>

		
		</Container>
		</div>
		
		)
}
