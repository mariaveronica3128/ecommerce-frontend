import React from "react";
import {
  Box,
  Container,
  Row,
  FooterLink,
  Heading,
  Footertext
} from "./FooterStyle";
  
const Footer = () => {
  return (


    <Box className="footer mt-5  mb-0 pt-5" fixed="bottom">
      <Container>
        <Row>
          <Row>
            <Heading>Partners</Heading>
            <FooterLink href="https://www.google.com" target="_blank">
              <i className="fab fa-google">ADS</i>
            </FooterLink>
            <FooterLink href="https://zuitt.co/" target="_blank">
              <i className="fab fa-aws">Zuitt</i>
            </FooterLink>
           
          </Row>
          <Row>
            <Heading>Follow Us</Heading>
            <FooterLink href="https://www.facebook.com/graphixtee2022" target="_blank">
              <i className="fab fa-facebook-f">
                <span>
                  Facebook
                </span>
              </i>
            </FooterLink>
            
            <FooterLink href="https://www.instagram.com/graphixtee2022" target="_blank">
              <i className="fab fa-instagram">
                <span>
                  Instagram
                </span>
              </i>
            </FooterLink>

            <FooterLink href="https://www.twitter.com/graphixtee2022" target="_blank">
              <i className="fab fa-twitter">
                <span>
                  Twitter
                </span>
              </i>
            </FooterLink>

          </Row>
        </Row>
            <Footertext>All Rights Reserved 2022</Footertext>
      </Container>
    </Box>


  );
};
export default Footer;
