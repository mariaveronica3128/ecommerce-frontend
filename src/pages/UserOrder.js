// import React from 'react';
// import { useParams, useNavigate} from 'react-router-dom';
// import { Container, Row, Col, Button, Table } from 'react-bootstrap';
// import { useEffect, useState } from 'react';


// const admin = localStorage.getItem('admin')

// export default function Order(){

// 	const {orderId} = useParams();

// 	const [userId, setUserId] = useState("");
// 	const [totalAmount, setTotalAmount] = useState(0);
	
// 	const [price, setPrice] = useState("");
//     const [quantity, setQunatity] = useState("");
// 	const [purchasedOn, setPurchasedOn] = useState("");


// 	const navigate = useNavigate();

// 	useEffect(() => {
// 		fetch(`http:localhost:4000/orders/myOrders`, {
// 			method: "GET",
// 			headers: {
// 				"Authorization": `Bearer ${localStorage.getItem('accessToken')}`
// 			}
// 		})
// 		.then(result => result.json())
// 		.then(result => {
// 			if(result){
// 				setUserId(result.userId);
// 				setQunatity(result._quantity);
// 				setTotalAmount(result.totalAmount);
// 				setPrice(result.price);
				
// 				setPurchasedOn(result.purchasedOn);
				
// 			} else {
// 				alert(result.message);
// 				navigate('./error');
// 			}
// 		})

// 	},[])


// 	const orderBackground = () =>{
// 		if (admin === "true"){
// 			return "adminPage"
// 		} else {
// 			return "welcome"
// 		}
// 	}

// 	return(
// 		<Container fluid className={orderBackground()} style={{overflowY:'scroll'}}>
// 			<div style={{height:60}}/>
// 			<Row>
// 				<Col className="my-auto mb-4 mx-auto" md={6}>
// 					<h4><center><span className="loginLabel">ORDER: {orderId}</span></center></h4>
// 					<Table className="mt-3 tableBackground">
// 						<tbody>
// 							<tr className="whiteOpaque">
// 								<td><strong>Ordered By:</strong></td>
// 								<td>{userId}</td>
// 								<td><strong>Order Placed:</strong></td>
//                                 <td>{purchasedOn}</td>
							
// 							</tr>
							
// 							<tr>
// 								<td colSpan="4" className="tableHeader">
// 									<center><h5>Items:</h5></center>
//                                     <td>{price}</td>
// 								</td>
// 							</tr>
// 							<tr>
// 								<td>Product</td>
// 								<td>Price</td>
// 								<td>Quantity</td>
//                                 <td>{quantity}</td>
// 								<td>totalAmount</td>
// 							</tr>
							

// 							<tr>
// 								<td colSpan="4"><center><h5></h5></center></td>
// 							</tr>
// 							<tr>
// 								<td></td>
// 								<td></td>
// 								<td><strong>Total Amount</strong></td>
// 								<td>&#8369;{totalAmount.toFixed(2)}</td>
// 							</tr>

// 						</tbody>
// 					</Table>
// 				</Col>
// 			</Row>
// 			<Row>
// 				<Col>
// 					{	admin === "true"
// 						?<Button class="btn tableBtn" onClick={()=>navigate('/')}>Back to list</Button>
// 						:<Button class="btn tableBtn" onClick={()=>navigate('/products')}>Back to products</Button>
// 					}
// 				</Col>
// 			</Row>
// 		</Container>
// 	)
// }
