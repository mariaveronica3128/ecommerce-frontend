import React from 'react';
import {Row, Col, Card, Button, CardGroup} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Highlights(){
	return(


	<CardGroup >
	<Card border= "light" className = "p-4" >
		<Card.Img variant="top" src={require("../images/T13.jpg")} />
		<Card.Body>
		<Card.Title>T13</Card.Title>
		<Card.Text>
			Lucky Japanese Cat.
		</Card.Text>
		</Card.Body>
		<Card.Footer className= "footer">
		<Button className= "button"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
		</Card.Footer>
	</Card>
	<Card border= "light" className = "p-4">
		<Card.Img variant="top" src={require("../images/T61.jpg")} />
		<Card.Body>
		<Card.Title>T61</Card.Title>
		<Card.Text>
			Cute Sherlockfan Cat
		</Card.Text>
		</Card.Body>
		<Card.Footer className= "footer">
		<Button className= "button"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
		</Card.Footer>
	</Card>
	<Card border= "light" className = "p-4">
		<Card.Img variant="top" src={require("../images/T9.jpg")} />
		<Card.Body>
		<Card.Title>T9</Card.Title>
		<Card.Text>
			Serious Japanese Cat
		</Card.Text>
		</Card.Body>
		<Card.Footer className= "footer">
		<Button className= "button"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
		</Card.Footer>
	</Card>
	</CardGroup>


		);
}
