import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProducts({product, fetchData}) {

	//state for productId for the fetch URL
	const [productId, setProductId] = useState('');

	//Forms state
	//Add state for the forms of product
	const [ProductName, setProductName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	//state for editproduct Modals to open/close
	const [showEdit, setShowEdit] = useState(false)

	//function for opening the modal
	const openEdit = (productId) => {
		//to still get the actual data from the form
		fetch(`https://warm-shelf-88809.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			//populate all the input values with product info that we fetched
			setProductId(data._id);
			setProductName(data.ProductName);
			setDescription(data.description);
			setPrice(data.price)
		})

		//Then, open the modal
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setProductName('')
		setDescription('')
		setPrice(0)
	}

    // function to update the product
    const editProducts = (e) => {
        e.preventDefault();

        fetch(`https://warm-shelf-88809.herokuapp.com/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                ProductName: ProductName,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Successfully Update'
                })
            }else {
                Swal.fire ({
                    title: 'error',
                    icon: 'error',
                    text: 'Error Update'
                })
            }
            closeEdit();
            fetchData();
        })

    }

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}>Update</Button>

		{/*Edit Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProducts(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>UpdateProduct</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={ProductName} onChange={e => setProductName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}